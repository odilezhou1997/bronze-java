## Definition of *Done*

* All unit tests are passed.
* There is not *linting* error.
* Good code quality.
  * Meaningful names of `class`, variable and functions.
  * Using full name rather than abbreviation.
  * Using appropriate data structure.