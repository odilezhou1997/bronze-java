package com.tw.bronze.question1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class CollectionOperations {
    /**
     * This method will remove all the duplicated `String` values from a
     * collection.
     *
     * @param values The collection which may contain duplicated values
     * @return A new array removing the duplicated values.
     */
    public static String[] distinct(Iterable<String> values) throws IllegalArgumentException{
        // TODO:
        //  Please implement the method. If you have no idea what Iterable<T>
        //  is, please refer to the unit test, or you can have a search on
        //  the internet, or you can find it in our material.
        // <-start-
        if (values == null)
            throw new IllegalArgumentException();

        ArrayList<String> array = new ArrayList<String>();
        for (String value:values){
            array.add(value);
        }

        ArrayList <String> newArray = new ArrayList<>();

        Iterator <String> it = array.iterator();
        while (it.hasNext()) {
            String s = (String) it.next();

            if (!newArray.contains(s)){
                newArray.add(s);
            }
        }
        Collections.sort(newArray);
         return (String[]) newArray.toArray(new String[0]);
        // --end->
    }
}
