package com.tw.bronze.question2;

import java.util.HashMap;
import java.util.List;

public class ReceiptPrinter {
    private final HashMap<String, Product> products =
        new HashMap<>();

    public ReceiptPrinter(List<Product> products) {
        // TODO:
        //   Please implement the constructor
        // <-start-
        for (Product product:products) this.products.put(product.getId(),product);

        // --end->
    }

    public int getTotalPrice(List<String> selectedIds) {
        // TODO:
        //   Please implement the method
        // <-start-
        int totalPrice = 0;

        if (selectedIds == null){
            throw new IllegalArgumentException();
        } else if (selectedIds.isEmpty()){
            return totalPrice;
        } else {
            for (int i = 0; i < selectedIds.size(); i++) {
                String key = selectedIds.get(i);
                if (products.containsKey(key)){
                    totalPrice += this.products.get(key).getPrice();
                }
            }
        }
        return totalPrice;
        // --end->
    }
}

