package com.tw.bronze.question1;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class CollectionOperationsTest {
    @Test
    void should_remove_duplicates() {
        final String[] distinct = CollectionOperations.distinct(
            Arrays.asList("Hello", "Hello"));
        assertArrayEquals(new String[] { "Hello" }, distinct);
    }

    @Test
    void should_remove_duplicates_sorted() {
        final String[] distinct = CollectionOperations.distinct(
            Arrays.asList("Hello", "Hello", "Animal"));
        assertArrayEquals(new String[] { "Animal", "Hello" }, distinct);
    }

    @Test
    void should_return_empty_array_given_empty_array() {
        final String[] distinct = CollectionOperations.distinct(
            Collections.emptyList()
        );
        assertEquals(0, distinct.length);
    }

    @Test
    void should_throw_if_array_is_null() {
        assertThrows(
            IllegalArgumentException.class,
            () -> CollectionOperations.distinct(null));
    }
}
